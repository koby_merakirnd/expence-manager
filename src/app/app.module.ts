//system
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';

//third party
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';



//components
import { HeaderComponent, TableComponent } from './components/components';
//services
import {
  ModalService, RouteService, UserService, DataService
} from './services/services';

//modals
import { InfoModal, ErrorModal, ExpenseModal, EditModal } from './modals/modals';
import { ProfilePage, ReportsPage, StatsPage } from './pages/pages';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TableComponent,
    InfoModal,
    ErrorModal,
    ExpenseModal,
    EditModal,
    ProfilePage,
    ReportsPage,
    StatsPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    AppRoutingModule,
    MatCardModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule
  ],
  entryComponents: [
    InfoModal,
    ErrorModal,
    ExpenseModal,
    EditModal,
    ProfilePage,
    ReportsPage,
    StatsPage
  ],
  providers: [ModalService, RouteService, UserService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
