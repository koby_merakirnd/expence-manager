import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { constants } from '../maps/constants.map';
import * as _ from 'lodash';

@Injectable()
export class DataService {

  getData(name: string) {
      let observable = new Observable(observer => {
        let dataToReturn = localStorage.getItem(name);
        if(!dataToReturn && constants[name]) {
          dataToReturn = constants[name];
        } else {
          dataToReturn = JSON.parse(dataToReturn);
        }
        observer.next(dataToReturn);
      });
      return observable;
    }

    setData(name: string, data: any) {
      let observable = new Observable(observer => {
        localStorage.setItem(name, JSON.stringify(data));
        observer.next();
      });
      return observable;
    }

    updateData(name: string, data: any) {
      let observable = new Observable(observer => {
        this.getData(name).subscribe( expenseData => {
            let row = _.find(expenseData, {id: data.id});
            // apply the new changes.
            Object.assign(row, data);
            //save
            localStorage.setItem(name, JSON.stringify(expenseData));
        });
        let dataToUpdate = localStorage.getItem(name);
        dataToUpdate = JSON.parse(dataToUpdate);

        observer.next();
      });
      return observable;
    }

    removeData(name: string) {
      let observable = new Observable(observer => {
          localStorage.removeItem(name);
          observer.next();
      });
      return observable;
    }
}
