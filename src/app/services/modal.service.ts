
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalMap } from '../maps/modal.map';

@Injectable()
export class ModalService {
  constructor(public dialog: MatDialog){}

  public openDialog(type="info", data?:any) {
    let modal = ModalMap[type];
    if(!data.title) {
      data.title = modal.title;
    }
    const dialogRef = this.dialog.open(modal.element, {
      width: modal.width,
      height: modal.height,
      data: data
    });

    return dialogRef.afterClosed();
  }
}
