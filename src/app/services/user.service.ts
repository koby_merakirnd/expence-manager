import { Injectable } from '@angular/core';

export interface User {
  id?: Number,
  firstname: String,
  lastname: String,
  balance: Number,
  image: String
}

@Injectable()
export class UserService {
  private user: User = {
    id: 0,
    firstname: 'Albert',
    lastname: 'E.',
    balance: 1000,
    image: 'https://www.eternitynews.com.au/wp-content/uploads/2017/10/albert-einstein1_1000x563_acf_cropped.jpg'
  };
  public getUser(): User {
    let userToReturn: User = {
      firstname: this.user.firstname,
      lastname: this.user.lastname,
      balance: this.user.balance,
      image: this.user.image
    }
    return userToReturn;
  }

  public setUser(user: User) {
    this.user = user;
  }
}
