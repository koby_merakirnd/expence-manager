import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ModalService } from '../../services/modal.service';

const STATS_DATA_KEY = 'expenseData';
@Component({
  selector: 'stats-page',
  templateUrl: './stats.page.html',
  styleUrls: ['./stats.page.scss']
})

export class StatsPage implements OnInit {
  public data;
  public columns;
  constructor(private dataService: DataService, private modalService: ModalService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.dataService.getData(STATS_DATA_KEY).subscribe(data => {
      this.data = data;
      this.columns = this.getColumnNames(this.data);
    });
  }

  openEditModel(element) {
    this.modalService.openDialog('edit', element).subscribe( () => {
      //get refreshed data
      this.getData();
    })
  }

  getColumnNames(array) {
    let element = array[0];
    let keys = Object.keys(element);
    return keys;
  }
}
