import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface InfoData {
  title: string,
  message: string
}

@Component({
  selector: 'info-modal',
  templateUrl: './info.modal.html',
  styleUrls: ['./info.modal.scss']
})
export class InfoModal implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: InfoData) { }

  ngOnInit(): void { }
}
