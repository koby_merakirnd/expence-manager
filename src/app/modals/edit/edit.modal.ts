import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { constants } from '../../maps/constants.map';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'edit-modal',
  templateUrl: './edit.modal.html',
  styleUrls: ['./edit.modal.scss']
})
export class EditModal implements OnInit {
  public categories = constants.categories;
  public view : {
    title: string
  };
  public editForm = new FormGroup({
    id: new FormControl({value: null, disabled: true}, [Validators.required]),
    name: new FormControl(null, [Validators.required]),
    category: new FormControl(null, [Validators.required]),
    amount: new FormControl(null, [Validators.required])
  });

  constructor(
      @Inject(MAT_DIALOG_DATA) public data,
      private dataService: DataService,
      public dialogRef: MatDialogRef<any>
      ) {
    this.view = {
      title: data.title
    };
    delete data.title;
    this.editForm.patchValue(data);
  }

  onSubmit() {
    this.dataService.updateData('expenseData', this.editForm.value).subscribe( res => {
        //close the window
        this.dialogRef.close();
    });
  }

  ngOnInit(): void { }
}
