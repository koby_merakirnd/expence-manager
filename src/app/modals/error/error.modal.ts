import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'error-modal',
  templateUrl: './error.modal.html',
  styleUrls: ['./error.modal.scss']
})
export class ErrorModal implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }
  ngOnInit(): void { }
}
