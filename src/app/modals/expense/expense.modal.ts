import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'expense-modal',
  templateUrl: './expense.modal.html',
  styleUrls: ['./expense.modal.scss']
})
export class ExpenseModal implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void { }
}
