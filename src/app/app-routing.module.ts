import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfilePage } from './pages/profile/profile.page';
import { StatsPage } from './pages/stats/stats.page';
import { ReportsPage } from './pages/reports/reports.page';

const routes: Routes = [
  { path: '', component: ProfilePage },
  { path: 'stats', component: StatsPage },
  { path: 'reports', component: ReportsPage }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
