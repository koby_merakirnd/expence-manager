import { InfoModal } from '../modals/info/info.modal';
import { ErrorModal } from '../modals/error/error.modal';
import { ExpenseModal } from '../modals/expense/expense.modal';
import { EditModal } from '../modals/edit/edit.modal';
export const ModalMap = {
  info: {
    icon: 'info',
    title: 'Info Modal',
    height: '500px',
    width: '500px',
    element: InfoModal
  },
  error: {
    icon: 'error',
    title: 'Error Modal',
    height: '500px',
    width: '500px',
    element: ErrorModal
  },
  add: {
    icon: 'add_to_queue',
    title: 'Add Modal',
    height: '500px',
    width: '500px',
    element: EditModal
  },
  edit: {
    icon: 'edit',
    title: 'Edit Modal',
    height: '500px',
    width: '500px',
    element: EditModal
  }
};
