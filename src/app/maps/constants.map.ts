export const constants = {
  expenseData: [
    {
      id: 0,
      amount: 5,
      name: 'Milk',
      category: 'food'
    },
    {
      id: 1,
      amount: 10,
      name: 'Bread',
      category: 'food'
    }
    , {
      id: 2,
      amount: 20,
      name: 'Sugar',
      category: 'food'
    }
    , {
      id: 3,
      amount: 20,
      name: 'Kindergarten',
      category: 'home'
    }
    , {
      id: 4,
      amount: 200,
      name: 'gas',
      category: 'car'
    }
  ],
  categories: [
    {
      id: 0,
      value: 'food'
    },
    {
      id: 1,
      value: 'car'
    },
    {
      id: 2,
      value: 'home'
    }
  ]
};
