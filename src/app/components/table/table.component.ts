import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'table-component',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})

export class TableComponent implements OnInit, OnChanges {
  // public displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  @Input() public dataSource;
  @Input() public displayedColumns;
  @Output() editFunction = new EventEmitter<any>();
  public display = [];

  constructor() { }

  ngOnInit(): void {
  }

  edit(element) {
    console.log(element);
    //defensive programming.
    // in case edit function isn't defined
    if(this.editFunction) {
      this.editFunction.emit(element);
    } else {
      console.log('editFunction isn\'t defined');
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.display = this.displayedColumns;
    this.display.push('edit');
  }


}
